class Role < ActiveRecord::Base
    has_and_belongs_to_many :users
    
    def self.valid_role?(role)
        exists?(name: role)
    end
    
    def self.get_role(role)
       where(name: role).first 
    end
end
