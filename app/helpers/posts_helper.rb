module PostsHelper
    
    def error_count_message(error_count)
        
        "There #{(error_count>1) ? 'were' : 'was' } #{pluralize(error_count, 'error') } found "
    end
    
    def output_error_messages(error_messages)
       the_output = ""
       error_messages.each do |key, messages|
          messages.each do |msg|
            the_output = the_output + content_tag(:li, content_tag(:div, "#{key.to_s.titleize} #{msg}",
                                            class: 'alert alert-danger',
                                            role: 'alert')) 
          end
       end
        return the_output
    end
end
