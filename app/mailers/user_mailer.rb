class UserMailer < ActionMailer::Base
  default from: "noreply@custom-c9-fadhil1.c9.io"
  
  def welcome_email(user)
     @user = user
     @url = sign_in_url
     mail(to: @user.email, subject: 'Thanks for Registering!')
  end
end
