class PostsController < ApplicationController
  def index
    @posts = Post.all
  end

  def show
    @post = Post.find(params[:id])
  end
  
  def new
    @post = Post.new
    
  end
  
  def create
    @post = Post.new(valid_params)
    if @post.save
      redirect_to post_show_path(id: @post.id), notice: 'Successfully Created Post!'
    else
      render :new
    end
  end
  
  def valid_params
    params.require(:post).permit(:title, :body)
  end
end
