class User < ActiveRecord::Base
    validates :name, presence: true
    validates :email, presence: true, format: { with: /\A\S+@\S+\.\S+\z/, message: 'invalid format' }

    mount_uploader :file, FileUploader
    
    has_secure_password
    
    has_many :cats
    
    has_and_belongs_to_many :roles
    
    accepts_nested_attributes_for :cats, allow_destroy: true
    
    accepts_nested_attributes_for :roles, allow_destroy: true
    
    def add_role(role_name)
      role = Role.where(name: role_name).first
      if role && !self.roles.include?(role)
          self.roles.push(role)
      end
    end
    
    def remove_role(role_name)

       role = Role.where(name: role_name).first
        if role && self.roles.include?(role)
           self.roles.delete(role) 
        end
    end
    
    def has_role?(role_name)
       self.roles.include?(Role.where(name: role_name).first) 
    end
end

