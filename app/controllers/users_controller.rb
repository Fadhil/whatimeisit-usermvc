class UsersController < ApplicationController
  include SessionsHelper
  #before_filter :verify_admin, only: [:index]
  load_and_authorize_resource
  
  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    
    if @user.save
      #UserMailer.welcome_email(@user).deliver
      redirect_to user_path(id: @user.id), notice: "Successfully Signed Up"
    else

      render :new
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
    
    # if @user.cats.count == 0
    #   @user.cats.build
    # end
  end

  def update
    @user = User.find(params[:id])
    
    if @user.update_attributes(user_params)
      redirect_to user_path(id: @user.id), notice: 'Successfully Updated User'
    else
      render :edit
    end
  end

  def destroy
    @user = User.find(params[:id])
    
    if @user.destroy
      redirect_to users_path, notice: 'Successfully deleted User'
    else
      render :index
    end
  end
  
  def edit_role
    @user = User.find(params[:id])
  end
  
  def add_role
    @user = User.find(params[:id])
    role_change_messages = ""
    roles = params[:user][:roles_attributes]
    roles.each do |key,role|
      if role['_destroy'] == "1"
        @user.remove_role(role['name'])
        role_change_messages += "Removed Role #{role['name']}. "
      else
        @user.add_role(role['name'])
        
        role_change_messages += "Added Role #{role['name']}. "
      end
    end
    redirect_to :back, notice: role_change_messages

  end

  private
  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :file, :file_cache, :remove_file, cats_attributes: [:id, :name, :colour, :_destroy])
  end
  
  def verify_admin
    if user_logged_in?
      if !current_user.has_role?('Admin')
        redirect_to (request.referrer || root_path), notice: 'You need to be admin to view that page'
      end
    else
      redirect_to (request.referrer || root_path), notice: 'You need to be admin to view that page'
    end
  end
end
